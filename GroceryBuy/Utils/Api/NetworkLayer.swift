//
//  NetworkLayer.swift
//  GroceryBuy
//
//  Created by Shenll on 11/07/23.
//

import SystemConfiguration
import Foundation
import UIKit

enum Router {

    case userLogin
    case verifyOTP
    case resendOTP
    case signUp
    case banner
    case categories
    case imageBase
    case subCategory
    case accessToken
    case userProduct

    var scheme: String {
        switch self {
        default:
            return "https"
        }
    }

    var offerImageURL: String {
        switch self {
        default:
            return "/categeoryName/imageUrl_1687258024823_section_3.webp"
        }
    }

    var imageBaseURL: String {
        switch self {
        default:
            return "https://grocery.shenll.in/grocery/assets/"
        }
    }
    
    var host: String {
        switch self {
        default:
            return "grocery.shenll.in"
        }
    }

    var path: String {
        switch self {
        case .userLogin:
            return "/grocery/auth/user/login"
        case .verifyOTP:
            return "/grocery/auth/user/verifyOtp"
        case .resendOTP:
            return "/grocery/auth/user/resendOtp"
        case .signUp:
            return "/grocery/auth/user/signup"
        case .banner:
            return "/grocery/banner"
        case .categories:
            return "/grocery/category"
        case .imageBase:
            return "https://grocery.shenll.in/grocery/assets/"
        case .subCategory:
            return "/grocery/subcategory"
        case .accessToken:
            return "/auth/accessToken"
        case .userProduct:
            return "/grocery/product/user/product"
        }
    }

    var method: String {
        switch self {
        default:
            return "POST"
        }
    }

    var token: String? {
        return  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0YWNmMDY0ZWFmNGY0NGI2MmI1Y2E0YyIsIm5hbWUiOiJrayIsImVtYWlsIjoia2FsYWlzZWx2aS5zaGVubGxAZ21haWwuY29tIiwibW9iaWxlTnVtYmVyIjoiOTc5MTMwODY2MyIsInJvbGUiOiJjdXN0b21lciIsInN0YXR1cyI6IkFDVElWRSIsImlhdCI6MTY4OTIyNDY0MiwiZXhwIjoxNjg5ODI5NDQyfQ.3P_0weZnPHr7HFHr9upw7k1znXhIkpIcABMOB0ZUZfc"
    }

}


class ServiceLayer : NSObject, URLSessionDelegate {

    func downloadImage(from url: URL, completion: @escaping (Result<UIImage, Error>, Data?) -> Void) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return}
            DispatchQueue.main.async() { 
                completion(.success(image), nil)
            }
        }.resume()
    }

    func request<T: Codable>(router: Router, parameters: [String: Any]?  = nil,param: String? = nil,dataParam: Data? = nil, method: String? = nil,isToken: Bool? = false, completion: @escaping (Result<T, Error>, Data?) -> Void) {
        guard self.isConnectedToNetwork() else {
            AlertUtil.displayError(message: "Please check your internet connection")
            let loader = LoaderView()
            loader.hide()
            return
        }
        guard let base = UIApplication.shared.windows.filter({$0.isKeyWindow}).first?.rootViewController else {return}
        base.loadLoader(base.view, load: true)
        var components = URLComponents()
        components.scheme = router.scheme
        components.host = router.host
        components.path = router.path
        guard let url = components.url else { return }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method != nil ? method : router.method
        if(method == nil) {
            if let par = parameters {
                let postData = try! JSONSerialization.data(withJSONObject: par, options: .fragmentsAllowed)
                urlRequest.httpBody = postData as Data
            } else if let data = dataParam {
                urlRequest.httpBody = data
            } else {
                let postData = param?.data(using: .utf8)
                urlRequest.httpBody = postData! as Data
            }
        }

        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        if(isToken ?? false) {
            if let token = router.token {
                urlRequest.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
            }
        }

        let session = URLSession.shared
        let task = session.dataTask(with: urlRequest, completionHandler: { data, response, error -> Void in
            guard response != nil, let data = data else {
                return
            }
            let str = String(decoding: data, as: UTF8.self)
            if(router.path == "/product/user/product"){
                print("sss:::\(str)")
                print(router.token,isToken, components.url)
            }
            do {
                let responseObject = try JSONDecoder().decode(T.self, from: data)
                DispatchQueue.main.async {
                    base.loadLoader(base.view, load: false)
                    completion(.success(responseObject), nil)
                }

            } catch {
                DispatchQueue.main.async {
                    base.loadLoader(base.view, load: false)
                    completion(.failure(error), data)
                }
            }
        })

        task.resume()
    }

}

extension URLComponents{
    var queryItemsDictionary : [String:Any]{
        set (queryItemsDictionary){
            self.queryItems = queryItemsDictionary.map {
                URLQueryItem(name: $0, value: "\($1)")
            }
        }
        get{
            var params = [String: Any]()
            return queryItems?.reduce([:], { (_, item) -> [String: Any] in
                params[item.name] = item.value
                return params
            }) ?? [:]
        }
    }
}

extension ServiceLayer {

    func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)

        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }

        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        if flags.isEmpty {
            return false
        }

        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)

        return (isReachable && !needsConnection)
    }

    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        let certificates: [Data] = {
            if let path = Bundle.main.path(forResource: "my_certificate_to_pin", ofType: "der") {
                do {
                    let data = try Data(contentsOf: URL(string: path)!)
                    return [data]
                } catch {
                    return []
                }
            } else {
                return []
            }
        }()
        if let trust = challenge.protectionSpace.serverTrust, SecTrustGetCertificateCount(trust) > 0 {
            if let certificate = SecTrustGetCertificateAtIndex(trust, 0) {
                let data = SecCertificateCopyData(certificate) as Data
                if certificates.contains(data) {
                    completionHandler(.useCredential, URLCredential(trust: trust))
                    return
                }
            }
        }
        completionHandler(URLSession.AuthChallengeDisposition.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
    }

    func urlSession(_ session: URLSession, didBecomeInvalidWithError error: Error?) {
        if let err = error {
            print("Error: \(err.localizedDescription)")
        } else {
            print("Error")
        }
    }
}

enum customError: Error {
    case notConnected
}


//MARK: - Async image download
extension ServiceLayer {

    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }

    func downloadImage(from url: URL) {
        print("Download Started")
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            // always update the UI from the main thread
            DispatchQueue.main.async() { [weak self] in
//                self?.imageView.image = UIImage(data: data)
            }
        }
    }
}


