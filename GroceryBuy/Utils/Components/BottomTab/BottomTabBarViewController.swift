//
//  BottomTabBarViewController.swift
//  GroceryBuy
//
//  Created by Shenll on 13/07/23.
//

import UIKit
import CoreLocation

class BottomTabBarViewController: UITabBarController, StoryboardInstantiate {
    
    static var storyboardName: String = "Home"


    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.configNavBar("Home")
        let viewFN = UIView(frame: CGRect.init(x: 0, y: 0, width: 115, height: 40))
        viewFN.backgroundColor = .clear

        let button3 = UIButton(frame: CGRect.init(x: 0, y: 12, width: 11, height: 14))
        button3.setImage(UIImage(named: "location"), for: .normal)

        let locationName = UIButton(frame: CGRect.init(x: 12, y: 8, width: 100, height: 20))
        locationName.setTitle("Location", for: .normal)

        viewFN.addSubview(locationName)
        viewFN.addSubview(button3)
        self.navigationItem.titleView = viewFN

        let favButton = UIButton(frame: CGRect.init(x: 0, y: 12, width: 20, height: 17))
        favButton.setImage(UIImage(named: "favorites"), for: .normal)
        let searchButton = UIButton(frame: CGRect.init(x: 40, y: 6, width: 28, height: 28))
        searchButton.setImage(UIImage(named: "search"), for: .normal)
        let rightButtons = UIView(frame: CGRect.init(x: 0, y: 0, width: 60, height: 40))
        rightButtons.addSubview(favButton)
        rightButtons.addSubview(searchButton)

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightButtons)
        fetchingLocation()
    }
    

    func fetchingLocation() {
        let locationManager = CLLocationManager()
        locationManager.requestAlwaysAuthorization()

        DispatchQueue.global().async {
            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
            }
        }
    }

}

extension BottomTabBarViewController : CLLocationManagerDelegate {

//    didupda
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        print("didUpdateLocations")
//        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
//        print("locations = \(locValue.latitude) \(locValue.longitude)")
//    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("didUpdateLocations")
    }

    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        print("manager")
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
            switch status {
            case .authorizedWhenInUse:
                manager.startUpdatingLocation()
                break
            case .authorizedAlways:
                manager.startUpdatingLocation()
                break
            case .denied:
                //handle denied
                break
            case .notDetermined:
                 manager.requestWhenInUseAuthorization()
               break
            default:
                break
            }

    }
}
