//
//  AlertUtil.swift
//  GroceryBuy
//
//  Created by Shenll on 12/07/23.
//

import UIKit

class AlertUtil {
    
    static func displayError(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let ok = UIAlertAction (title: "OK", style: UIAlertAction.Style.default) { action in
            alert.dismiss(animated: true)
        }
        alert.addAction(ok)
        guard let base = UIApplication.shared.windows.filter({$0.isKeyWindow}).first?.rootViewController else {return}
        base.present(alert, animated: true)
    }
}

