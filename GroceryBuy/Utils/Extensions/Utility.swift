//
//  Utility.swift
//  GroceryBuy
//
//  Created by Shenll on 12/07/23.
//

import UIKit
class Utility: NSObject {
    public static func validateLength(text : String, size : (min : Int, max : Int)) -> Bool{
        return (size.min...size.max).contains(text.count)
    }

   
}

