//
//  StoryboardInstantiate.swift
//  GroceryBuy
//
//  Created by Shenll on 11/07/23.
//

import UIKit

protocol StoryboardInstantiate: NSObject {
    static var storyboardName: String { get }
    static var identifier: String { get }
    static func instantiate() -> Self
}

extension StoryboardInstantiate {

    static var identifier: String {
        return String(describing: Self.self)
    }

    private static var storyboard: UIStoryboard {
        return UIStoryboard(name: Self.storyboardName, bundle: nil)
    }

    /// Instantiates view controller with the specified identifier.
    /// Unless specified it will take the **identifier** as the class name.
    static func instantiate() -> Self {
        return storyboard.instantiateViewController(withIdentifier: identifier) as! Self
    }


}
