//
//  UIViewControllerExt.swift
//  GroceryBuy
//
//  Created by Shenll on 11/07/23.
//

import UIKit
extension UIViewController {

    /// Call this once to dismiss open keyboards by tapping anywhere in the view controller
    func setupHideKeyboardOnTap() {
        self.view.addGestureRecognizer(self.endEditingRecognizer())
        self.navigationController?.navigationBar.addGestureRecognizer(self.endEditingRecognizer())
    }

    /// Dismisses the keyboard from self.view
    private func endEditingRecognizer() -> UIGestureRecognizer {
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(self.view.endEditing(_:)))
        tap.cancelsTouchesInView = false
        return tap
    }

    func loadLoader(_ view: UIView, load: Bool, caption: String? = nil){
        let loader = LoaderView()
        if(load) {
            if(caption?.count ?? 0 > 0) {
                loader.show(title: caption, animated: true)
            }
            else {
                loader.show(animated: true)
            }
        }
        else {
            loader.hide()
        }
    }

    func configNavBar(_ pageInfo: String) {

            let appearance = UINavigationBarAppearance()
            appearance.configureWithTransparentBackground()
            let gradient = CAGradientLayer()
            let sizeLength = UIScreen.main.bounds.size.width
            let defaultNavigationBarFrame = CGRect(x: 0, y: 0, width: sizeLength, height: 64)
            gradient.frame = defaultNavigationBarFrame
            gradient.colors = [ UIColor(named: "bg")?.cgColor ?? UIColor(), UIColor(named: "bg")?.cgColor ?? UIColor()]
            gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
            appearance.backgroundImage = self.image(fromLayer: gradient)
            appearance.shadowImage = UIImage()
            appearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
            navigationController?.navigationBar.standardAppearance = appearance
            navigationController?.navigationBar.setBackgroundImage( self.image(fromLayer: gradient), for: UIBarMetrics.default)
            navigationController?.navigationBar.shadowImage = UIImage()
            navigationController?.navigationBar.scrollEdgeAppearance = navigationController?.navigationBar.standardAppearance
            navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
            navigationController?.navigationBar.shadowImage = UIImage()
            navigationController?.toolbar.setShadowImage(UIImage(), forToolbarPosition: .any)
            navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            navigationController?.navigationBar.topItem?.title = "pageInfo"
            navigationController?.navigationBar.topItem?.titleView = UIView()
       
        navigationController?.navigationBar.barTintColor = .white
    }

    func configHomeNavBar(_ pageInfo: String) {
        self.configNavBar(pageInfo)
        let viewFN = UIView(frame: CGRect.init(x: 0, y: 0, width: 115, height: 40))
        viewFN.backgroundColor = .clear

        let button3 = UIButton(frame: CGRect.init(x: 0, y: 12, width: 11, height: 14))
        button3.setImage(UIImage(named: "location"), for: .normal)

        let locationName = UIButton(frame: CGRect.init(x: 12, y: 8, width: 100, height: 20))
        locationName.setTitle("Location", for: .normal)

        viewFN.addSubview(locationName)
        viewFN.addSubview(button3)
        self.navigationItem.titleView = viewFN

        let favButton = UIButton(frame: CGRect.init(x: 0, y: 12, width: 20, height: 17))
        favButton.setImage(UIImage(named: "favorites"), for: .normal)
        let searchButton = UIButton(frame: CGRect.init(x: 40, y: 6, width: 28, height: 28))
        searchButton.setImage(UIImage(named: "search"), for: .normal)
        let rightButtons = UIView(frame: CGRect.init(x: 0, y: 0, width: 60, height: 40))
        rightButtons.addSubview(favButton)
        rightButtons.addSubview(searchButton)

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightButtons)

    }

    func image(fromLayer layer: CALayer) -> UIImage {
        UIGraphicsBeginImageContext(layer.frame.size)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let outputImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return outputImage!
    }


}
