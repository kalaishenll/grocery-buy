//
//  SettingsViewController.swift
//  GroceryBuy
//
//  Created by Shenll on 13/07/23.
//

import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var mainView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        mainView.layer.borderWidth = 1
        mainView.layer.borderColor = UIColor(named: "bg")?.cgColor ?? UIColor().cgColor
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
