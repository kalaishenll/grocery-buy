//
//  ProductDetailViewController.swift
//  GroceryBuy
//
//  Created by Shenll on 17/07/23.
//

import UIKit
import RxSwift
import RxCocoa
import ETCarouSwift

class ProductDetailViewController: UIViewController, StoryboardInstantiate{

    static var storyboardName: String = "Product"
    var productDetails: ProductData!
    var bannerImages:[UIImage]  = [UIImage]()
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var additionalLabel: UILabel!
    @IBOutlet weak var imageMainView: UIView!
    @IBOutlet weak var qtyLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var offerLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configProductData()

    }

    func configProductData() {
        guard let detail = self.productDetails else {return}
        self.nameLabel.text = detail.productName
        descLabel.text = detail.productDescription
        additionalLabel.text = detail.additionalInfo
        qtyLabel.text = detail.productQuantity
        priceLabel.text = "\(detail.productPrice ?? 0)"
        ratingLabel.text = detail.productRating
        guard let pp = productDetails.productPrice else {return}
        guard let sp = productDetails.salePrice else {return}
        let offer = pp-sp
        offerLabel.text = "\(offer) %"
        guard let imgs = productDetails.productImage else {return}
        bannerImages = [UIImage]()
        self.displayImage(router: Router.imageBase, bannerList: imgs)


    }

    func displayImage(router: Router,bannerList: [ProductImage]) {

        for res in bannerList {
            guard let imag = res.imgURL else {return}
            let url = "\(router.imageBaseURL)\(imag)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            print("\(url)")
            let ser = ServiceLayer()
            guard let iUrl = URL(string: url ?? "") else {return}
            ser.downloadImage(from: iUrl, completion: { (result: Result<UIImage, Error>, _) in
                switch result {
                case .success(let image):
                    self.bannerImages.append(image)
                    print("cc",bannerList.count,self.bannerImages.count)
                    if(self.bannerImages.count == bannerList.count) {
                        self.productImageUI()
                    }
                    break
                case .failure(_):
                    break
                }
            })
        }
        
    }

    func productImageUI() {
        let frame = CGRect(x: 0, y: 0, width: imageMainView.frame.self.width , height: imageMainView.frame.self.height)
         let carouView = CarouView(frame: frame, imageSet: bannerImages, rideDirection: .leftToRight)
        carouView.layer.masksToBounds = true
        carouView.layer.cornerRadius = 17
        self.imageMainView.addSubview(carouView)
    }



}
