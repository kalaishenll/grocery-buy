//
//  CategoryCollectionViewCell.swift
//  GroceryBuy
//
//  Created by Shenll on 14/07/23.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var mainView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()

        mainView.shadow()
        configUI()
    }

    func configUI (){
        mainView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.showSubCategory(_:)))
//        mainView.addGestureRecognizer(tap)
    }

    @objc func showSubCategory(_ id: UITapGestureRecognizer) {
        print("showSubCategory")
    }
}
