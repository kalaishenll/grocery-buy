//
//  HomeViewModel.swift
//  GroceryBuy
//
//  Created by Shenll on 14/07/23.
//

import Foundation
import UIKit
import RxSwift

class HomeViewModel: NSObject {

    private let serviceLayer = ServiceLayer()
    var bannerResult = PublishSubject<BannerModel>()
    var categoryResult = PublishSubject<CategoryModel>()

    func getBanners() {
        serviceLayer.request(router: Router.banner, parameters: nil, method: "GET") { (result: Result<BannerModel, Error>, _) in
            switch result {
            case .success(let details):
                DispatchQueue.main.async {
                    self.bannerResult.onNext(details)
                }
            case .failure(let error):
                self.bannerResult.onError(error)
                break
            }
        }
    }

    func getCategories() {
        serviceLayer.request(router: Router.categories, parameters: nil, method: "GET") { (result: Result<CategoryModel, Error>, _) in
            switch result {
            case .success(let details):
                DispatchQueue.main.async {
                    self.categoryResult.onNext(details)
                }
            case .failure(let error):
                self.categoryResult.onError(error)
                break
            }
        }
    }


}
