//
//  HomeViewController.swift
//  GroceryBuy
//
//  Created by Shenll on 12/07/23.
//

import UIKit
import RxSwift
import RxCocoa
import ETCarouSwift

class HomeViewController: UIViewController, StoryboardInstantiate {
    
    static var storyboardName: String = "Home"
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var bannerView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var offerImageView: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!

    private var viewModel = HomeViewModel()
    let disposeBag = DisposeBag()
    var categoryItems = [CategoryList]()
    var bannerImages:[UIImage]  = [UIImage]()

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.getBanners()
        viewModel.getCategories()
        self.viewModelObservers()
        configUI()
    }



    func configUI(){
        collectionView.register(UINib(nibName: "CategoryCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "category_cell")
        mainView.layer.borderWidth = 1
        mainView.layer.borderColor = UIColor(named: "bg")?.cgColor ?? UIColor().cgColor
        searchBar.barTintColor = UIColor.white
        searchBar.setBackgroundImage(UIImage.init(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        self.collectionView.isUserInteractionEnabled = true
    }

    func viewModelObservers(){
        viewModel.bannerResult.subscribe({ [weak self] response in
            guard self != nil else {
                return
            }
            switch response {
            case let .next(res):
                switch res.status {
                case statusValue.ok.rawValue :
                    guard let bannerList = res.data?.bannerlist else {return }
                    self?.displayImage(router: Router.resendOTP, bannerList: bannerList)
                case statusValue.error.rawValue:
                    AlertUtil.displayError(message: "Erro")
                case .none:
                    break
                case .some(_):
                    break
                }
                break
            case let .error(error):
                AlertUtil.displayError(message: error.localizedDescription)
                break
            case .completed:
                break
            }
        }).disposed(by: disposeBag)

        viewModel.categoryResult.subscribe({ [weak self] response in
            guard self != nil else {
                return
            }
            switch response {
            case let .next(res):
                switch res.status {
                case statusValue.ok.rawValue :
                    guard let category = res.data?.results else {return }
                    self?.categoryItems = category
                    self?.collectionView.reloadData()
                    //                    let items = Observable.just(category).bind(to: collectionView.rx.items)
                    //                    {(tv, row, item) -> UICollectionViewCell in
                    //
                    //                        if item.userId == 0 {
                    //                            let cellIdentifier = R.reuseIdentifier.meChatCell.identifier
                    //                            let cell = tv.dequeueReusableCell(withIdentifier: cellIdentifier, for: IndexPath.init(row: row, section: 0)) as! MeChatCell
                    //                            cell.vm = item
                    //                            return cell
                    //                        }
                    //                    }
                case statusValue.error.rawValue:
                    AlertUtil.displayError(message: "Erro")
                case .none:
                    break
                case .some(_):
                    break
                }
                break
            case let .error(error):
                AlertUtil.displayError(message: error.localizedDescription)
                break
            case .completed:
                break
            }
        }).disposed(by: disposeBag)
    }

    func displayImage(router: Router,bannerList: [BannerList]) {
        self.offerImageView.downloaded(from: "\(router.imageBaseURL)\(router.offerImageURL)")
        for res in bannerList {
            guard let imag = res.bannerImage else {return}
            let url = "\(router.imageBaseURL)\(imag)"
            let ser = ServiceLayer()
            guard let iUrl = URL(string: url) else {return}
            ser.downloadImage(from: iUrl, completion: { (result: Result<UIImage, Error>, _) in
                switch result {
                case .success(let image):
                    self.bannerImages.append(image)
                    if(self.bannerImages.count == 4) {
                        self.pageUI()
                    }
                    break
                case .failure(_):
                    break
                }
            })
        }

//        guard let imageURL =  bannerList[0].bannerImage else {return}
//        let url = "\(router.imageBaseURL)\(imageURL)"
//        DispatchQueue.main.async {
//            self.imageView.downloaded(from: url)
//            self.offerImageView.downloaded(from: "\(router.imageBaseURL)\(router.offerImageURL)")
//        }



    }

    func pageUI () {

        let frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 24 , height: 145)
         let carouView = CarouView(frame: frame, imageSet: bannerImages, rideDirection: .leftToRight)
        carouView.layer.masksToBounds = true
        carouView.layer.cornerRadius = 17

        self.bannerView.addSubview(carouView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }

    @IBAction func onTap(_ id: UIButton) {
        print("onTap")
    }

}

//MARK: - UICollectionView delegates

extension HomeViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.categoryItems.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "category_cell", for: indexPath as IndexPath) as! CategoryCollectionViewCell
        cell.nameLabel.text = self.categoryItems[indexPath.row].categoryName
        let rou = Router.imageBase
        guard let url = self.categoryItems[indexPath.row].categoryIcon else {return UICollectionViewCell()}
        guard let url1 = "\(rou.imageBaseURL)\(url)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {return UICollectionViewCell()}
        cell.imageView.downloaded(from:  url1)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let product = ProductListViewController.instantiate()
        product.selectedCategory = self.categoryItems[indexPath.row]
        self.navigationController?.pushViewController(product, animated: true)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (UIScreen.main.bounds.width / 4)-20, height: 120)
    }

}
