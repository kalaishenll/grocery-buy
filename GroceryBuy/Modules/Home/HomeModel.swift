//
//  HomeModel.swift
//  GroceryBuy
//
//  Created by Shenll on 14/07/23.
//

import Foundation

//MARK: - Banner
struct BannerModel: Codable {
    var status: String?
    var data: BannerData?
}

struct BannerData: Codable {
    var bannerlist: [BannerList]?
}

struct BannerList: Codable {
    var _id: String?
    var bannerImage: String?
}
//MARK: - Categories
struct CategoryModel: Codable {
    var status: String?
    var data: CategoryData?
}

struct CategoryData: Codable {
    var results: [CategoryList]?
}

struct CategoryList: Codable {
    var _id: String?
    var categoryName: String?
    var categoryIcon: String?
}
