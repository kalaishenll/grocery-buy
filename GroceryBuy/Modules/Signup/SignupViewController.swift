//
//  Signup.swift
//  GroceryBuy
//
//  Created by Shenll on 11/07/23.
//

import UIKit
import RxSwift
import RxCocoa

class SignupViewController: UIViewController {

    @IBOutlet weak var signupButton : UIButton!
    @IBOutlet weak var creatAccountLabel : UILabel!
    @IBOutlet weak var nameField : UITextField!
    @IBOutlet weak var mobileNumberField : UITextField!
    @IBOutlet weak var emailField : UITextField!
    private var viewModel = LoginViewModel()
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()


        creatAccountLabel.attributedText = NSAttributedString(string: "Already have an account? Login", attributes:
                                                                [.underlineStyle: NSUnderlineStyle.single.rawValue])
        mobileNumberField.keyboardType = .numberPad
        self.setupHideKeyboardOnTap()
        self.navigationController?.isNavigationBarHidden = true
        nameField.text = "Test"
        mobileNumberField.text = "0000"
        emailField.text = "test@t.in"
        signupAction()
        viewModelObservers()
    }

    func viewModelObservers(){
        viewModel.loginResult.subscribe({ [weak self] response in
            guard self != nil else {
                return
            }
            switch response {
            case let .next(res):
                switch res.status {
                case statusValue.ok.rawValue :
                    let otp: OTPViewController = OTPViewController.instantiate()
                    otp.mobileNumber = self?.mobileNumberField.text ?? ""
                    self?.navigationController?.pushViewController(otp, animated: true)
                case statusValue.error.rawValue:
                    AlertUtil.displayError(message: res.message ?? "")
                case .none:
                    break
                case .some(_):
                    break
                }
                break
            case let .error(error):
                AlertUtil.displayError(message: error.localizedDescription)
                break
            case .completed:
                break
            }
        }).disposed(by: disposeBag)
        
    }

    //MARK: - implementation of verify OTP button
    func signupAction() {
        signupButton.rx.tap.do(onNext:  { [unowned self] in
            self.emailField.resignFirstResponder()
        }).subscribe(onNext: { [unowned self] in
            guard let name  = nameField.text, let number = mobileNumberField.text, let email = emailField.text else {return}
            if !self.viewModel.validateName(value: name) {
                AlertUtil.displayError(message: "Please enter valid Name")
            }
            else if !self.viewModel.validateMobileNumber(value: number) {
                AlertUtil.displayError(message: "Please enter valid Mobile number")
            }
            else if !self.viewModel.validateEmail(value: email) {
                AlertUtil.displayError(message: "Please enter valid Email")
            }
            else {
                viewModel.signUP(name: name, email: email, mobilenumber: number)
            }

        }).disposed(by: disposeBag)
    }

}
