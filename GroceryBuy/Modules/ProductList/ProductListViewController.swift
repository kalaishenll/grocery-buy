//
//  ProductListViewController.swift
//  GroceryBuy
//
//  Created by Shenll on 17/07/23.
//

import UIKit
import RxSwift
import RxCocoa

class ProductListViewController: UIViewController, StoryboardInstantiate{

    static var storyboardName: String = "Product"
    var selectedCategory: CategoryList!
    private var viewModel = ProductViewModel()
    let disposeBag = DisposeBag()
    var subCategoryItems = [SubCategoryList]()
    var productItems = [ProductData]()
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var subCategoryCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard let selectedCateg = self.selectedCategory else {return}
        print("sele:",selectedCateg)

        configUI()
        viewModelConfig()
        
    }

    func configUI() {
        collectionView.register(UINib(nibName: "ProductCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "product_cell")
//        subCategoryCollectionView.register(UINib(nibName: "ProductCategoryCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "product_category")
        subCategoryCollectionView.isPagingEnabled = true
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        subCategoryCollectionView.collectionViewLayout = layout
    }

}

//MARK: - Viewmodel methods
extension ProductListViewController {


    func viewModelConfig(){
        viewModel.getSubCategories()
        viewModel.subCategoryResult.subscribe({ [weak self] response in
            guard self != nil else {
                return
            }
            switch response {
            case let .next(res):
                switch res.status {
                case statusValue.ok.rawValue :
                    guard let category = res.data?.subCategories else {return }
                    self?.subCategoryItems = category
                    print("subCategoryItems::",category.count)
//                    self?.collectionView.reloadData()
                    self?.subCategoryCollectionView.reloadData()
                    self?.viewModel.getProduct()
                case statusValue.error.rawValue:
                    AlertUtil.displayError(message: "Erro")
                case .none:
                    break
                case .some(_):
                    break
                }
                break
            case let .error(error):
                AlertUtil.displayError(message: error.localizedDescription)
                break
            case .completed:
                break
            }
        }).disposed(by: disposeBag)

        // Observers for products
        viewModel.productResult.subscribe({ [weak self] response in
            guard self != nil else {
                return
            }
            switch response {
            case let .next(res):
                switch res.status {
                case statusValue.ok.rawValue :
                    guard let category = res.data else {return }
                    self?.productItems = category
                    print("productItems::",self?.productItems.count)
                    self?.collectionView.reloadData()
                case statusValue.error.rawValue:
                    AlertUtil.displayError(message: "Erro")
                case .none:
                    break
                case .some(_):
                    break
                }
                break
            case let .error(error):
                AlertUtil.displayError(message: error.localizedDescription)
                break
            case .completed:
                break
            }
        }).disposed(by: disposeBag)
    }
}

//MARK: - UICollectionView delegates

extension ProductListViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionView == subCategoryCollectionView ? self.subCategoryItems.count :  self.productItems.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case subCategoryCollectionView:
            return bindSubCategoryCells(indexPath: indexPath)
        case collectionView:
            return self.bindProductCells1(indexPath: indexPath)
        default:
            return UICollectionViewCell()
        }
    }

    func bindProductCells(indexPath: IndexPath)->UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "product_cell", for: indexPath as IndexPath) as! ProductCollectionViewCell
        cell.bindValues(val: self.subCategoryItems[indexPath.row])
        return cell
    }

    func bindProductCells1(indexPath: IndexPath)->UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "product_cell", for: indexPath as IndexPath) as! ProductCollectionViewCell
        cell.bindValues(val: self.productItems[indexPath.row])
        return cell
    }

    func bindSubCategoryCells(indexPath: IndexPath)->UICollectionViewCell {
        guard let cell: SubCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "product_cell", for: indexPath as IndexPath) as? SubCollectionViewCell  else {return UICollectionViewCell()}
//        cell.bindValues(val: self.subCategoryItems[indexPath.row])
        cell.backgroundColor = .red
        return cell
    }


    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        let detail = ProductDetailViewController.instantiate()
        detail.productDetails = self.productItems[indexPath.row]
        self.navigationController?.pushViewController(detail, animated: true)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView == subCategoryCollectionView ? CGSize(width: 74, height: 100) : CGSize(width: (self.collectionView.frame.size.width / 2)-20, height: 268)
    }

}
