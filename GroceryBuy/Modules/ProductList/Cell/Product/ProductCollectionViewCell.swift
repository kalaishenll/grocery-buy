//
//  ProductCollectionViewCell.swift
//  GroceryBuy
//
//  Created by Shenll on 17/07/23.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var offerLabel: UILabel!
    @IBOutlet weak var favImageView: UIImageView!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.shadow()
        configUI()
    }

    func configUI (){
//        mainView.backgroundColor  = .red
        favImageView.image = UIImage(named: "favorites")?.withTintColor(UIColor(named: "bg") ?? UIColor.lightGray)
        mainView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.showSubCategory(_:)))
//        mainView.addGestureRecognizer(tap)
    }

    @objc func showSubCategory(_ id: UITapGestureRecognizer) {
        print("showSubCategory")
    }

    func bindValues(val : SubCategoryList) {
        self.nameLabel.text = val.subCategoryName
        let rou = Router.imageBase
        guard let url = val.subCategoryIcon else {return}
        guard let url1 = "\(rou.imageBaseURL)\(url)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {return }
        imageView.downloaded(from:  url1)
        
    }

    func bindValues(val : ProductData) {
        self.nameLabel.text = val.productName
        let rou = Router.imageBase
        guard let url = val.productImage?[0].imgURL else {return}
        guard let url1 = "\(rou.imageBaseURL)\(url)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {return }
        imageView.downloaded(from:  url1)

    }
    
}
