//
//  SubCollectionViewCell.swift
//  GroceryBuy
//
//  Created by Shenll on 17/07/23.
//

import UIKit

class SubCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func bindValues(val : SubCategoryList) {
        self.nameLabel.text = val.subCategoryName
        let rou = Router.imageBase
        guard let url = val.subCategoryIcon else {return}
        guard let url1 = "\(rou.imageBaseURL)\(url)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {return }
        imageView.downloaded(from:  url1)

    }

}
