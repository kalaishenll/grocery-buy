//
//  ProductViewModel.swift
//  GroceryBuy
//
//  Created by Shenll on 17/07/23.
//

import Foundation
import UIKit
import RxSwift

class ProductViewModel: NSObject {

    private let serviceLayer = ServiceLayer()
    var subCategoryResult = PublishSubject<SubCategoryModel>()
    var accessToken = PublishSubject<AccessToken>()
    var productResult = PublishSubject<ProductModel>()

    func getSubCategories() {
        serviceLayer.request(router: Router.subCategory, parameters: nil, method: "GET") { (result: Result<SubCategoryModel, Error>, _) in
            switch result {
            case .success(let details):
                DispatchQueue.main.async {
                    self.subCategoryResult.onNext(details)
                }
            case .failure(let error):
                self.subCategoryResult.onError(error)
                break
            }
        }
    }

    func getAccessToken() {
        serviceLayer.request(router: Router.accessToken, parameters: nil, method: "GET") { (result: Result<AccessToken, Error>, _) in
            switch result {
            case .success(let details):
                DispatchQueue.main.async {
                    self.accessToken.onNext(details)
                }
            case .failure(let error):
                self.accessToken.onError(error)
                break
            }
        }
    }

    func getProduct() {
        serviceLayer.request(router: Router.userProduct, parameters: nil, method: "GET",isToken: true) { (result: Result<ProductModel, Error>, _) in
            switch result {
            case .success(let details):
                DispatchQueue.main.async {
                    self.productResult.onNext(details)
                }
            case .failure(let error):
                self.productResult.onError(error)
                break
            }
        }
    }


}
