//
//  ProductModel.swift
//  GroceryBuy
//
//  Created by Shenll on 17/07/23.
//

import Foundation

//MARK: - Categories
struct SubCategoryModel: Codable {
    var status: String?
    var data: SubCategoryData?
}

struct SubCategoryData: Codable {
    var subCategories: [SubCategoryList]?
}

struct SubCategoryList: Codable {
    var _id: String?
    var subCategoryName: String?
    var subCategoryIcon: String?
    var categoryId: SubCategory?
}
struct SubCategory: Codable {
    var _id: String?
    var categoryName: String?
    var categoryIcon: String?
}

//MARK: - UserProduct
struct AccessToken: Codable {
    var status: String?
    var data: AccessData?

}
struct AccessData: Codable {
    var token: String?
}

//MARK: - Product
struct ProductModel: Codable {
    var status: String?
    var data: [ProductData]?
}

struct ProductData: Codable {
    var _id: String?
//    var categoryId: Category?
//    var subCategoryId: SubCategoryw?
    var productName: String?
    var productImage: [ProductImage]?
    var productDescription: String?
    var productQuantity: String?
    var productPrice: Int?
    var salePrice: Int?
    var additionalInfo: String?
    var productRating: String?

}
struct ProductImage: Codable {
    var imgURL: String?
}

struct Category: Codable {
    var _id: String?
    var categoryName: String?
    var categoryIcon: String?

}
struct SubCategoryw: Codable {
    var _id: String?
    var categoryId: String?
    var subCategoryName: String?
    var subCategoryIcon: String?
}

