//
//  OrderViewController.swift
//  GroceryBuy
//
//  Created by Shenll on 13/07/23.
//

import UIKit

class OrderViewController: UIViewController, StoryboardInstantiate {
    
    static var storyboardName: String = "Home"
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var bannerCollectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        mainView.layer.borderWidth = 1
        mainView.layer.borderColor = UIColor(named: "bg")?.cgColor ?? UIColor().cgColor

        bannerCollectionView.register(UINib(nibName: "CategoryCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: "category_cell")
        bannerCollectionView.isScrollEnabled = true
        bannerCollectionView.isUserInteractionEnabled = true
        bannerCollectionView.alwaysBounceHorizontal = true
//        bannerCollectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 120, right: 0)
        bannerCollectionView.isPagingEnabled = true
        let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
        bannerCollectionView.collectionViewLayout = layout
        bannerCollectionView.contentSize = CGSize(width: CGFloat(3 * Int(336)), height: 150)
        bannerCollectionView.contentOffset = CGPoint(x: CGFloat(3 * Int(336)), y: 0)

        bannerCollectionView.isScrollEnabled = true
        bannerCollectionView.contentSize = CGSize(width: CGFloat(3 * Int(336)), height: 150)
        bannerCollectionView.contentOffset = CGPoint(x: CGFloat(3 * Int(336)), y: 0)
        bannerCollectionView.isPagingEnabled = true
        bannerCollectionView.alwaysBounceHorizontal = false
        bannerCollectionView.bounces = false
        bannerCollectionView.showsHorizontalScrollIndicator = false


//        bannerCollectionView.isPagingEnabled = true
//        bannerCollectionView.isScrollEnabled = true
//        bannerCollectionView.setContentOffset(CGPoint(x: 0,y: 0), animated: true)
    }



}

//MARK: - UICollectionView delegates

extension OrderViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "category_cell", for: indexPath as IndexPath) as! CategoryCollectionViewCell
        cell.nameLabel.text = "Name:\(indexPath.row)"
        _ = Router.imageBase
//        let url = "https://grocery.shenll.in/grocery/assets//categeoryName/imageUrl_1687257814356_fruits_vegetables%20(1).webp"
////        guard let url1 = "\(rou.imageBaseURL)\(url)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {return UICollectionViewCell()}
//        cell.imageView.downloaded(from:  url)\
        cell.imageView.backgroundColor = .red
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("\(indexPath.item)!")
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (UIScreen.main.bounds.width)-40, height: 100)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
    }

}
