//
//  OfferViewController.swift
//  GroceryBuy
//
//  Created by Shenll on 13/07/23.
//

import UIKit
import ETCarouSwift

class OfferViewController: UIViewController {

    @IBOutlet weak var mainView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        mainView.layer.borderWidth = 1
        mainView.layer.borderColor = UIColor(named: "bg")?.cgColor ?? UIColor().cgColor
        pageUI()
    }
    

    func pageUI () {
        let images:[UIImage] = [UIImage(named: "basket-full-vegetables 1")!,
                                UIImage(named: "splashscreen")!,
                                UIImage(named: "basket-full-vegetables 1")!,
                                ]

         let frame = CGRect(x: 10, y: 100, width: 300, height: 200)

         let carouView = CarouView(frame: frame, imageSet: images, rideDirection: .leftToRight)
        self.view.addSubview(carouView)
    }

}
