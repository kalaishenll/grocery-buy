//
//  LoginViewModel.swift
//  GroceryBuy
//
//  Created by Shenll on 11/07/23.
//

import UIKit
import RxSwift
enum statusValue: String {
    case ok = "ok"
    case error = "error"
}
class LoginViewModel: NSObject {

    private let serviceLayer = ServiceLayer()
    var loginResult = PublishSubject<LoginModel>()
    var otpResult = PublishSubject<OTPModel>()

    func validateMobileNumber(value: String) -> Bool{
        guard Utility.validateLength(text: value, size: (10,10)) else{
            return false;
        }
        return true
    }
    func validateName(value: String) -> Bool{
        guard Utility.validateLength(text: value, size: (3,20)) else{
            return false;
        }
        return true
    }
    func validateEmail(value: String) -> Bool{
        return NSPredicate(format: "SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}").evaluate(with: value)
    }



    func loginUser(mobilenumber: String) {
        let params = ["mobileNumber":mobilenumber, "type":"LOGIN"]
        serviceLayer.request(router: Router.userLogin, parameters: params) { (result: Result<LoginModel, Error>, _) in
            switch result {
            case .success(let details):
                DispatchQueue.main.async {
                    self.loginResult.onNext(details)
//                    self.loginResult.onCompleted()
                }
            case .failure(let error):
                self.loginResult.onError(error)
                break
            }
        }
    }

    func verifyOTP(otp: String, mobilenumber: String) {
        let otpi = Int(otp)
        let params = ["mobileNumber":mobilenumber,"code": otpi ?? 0, "type":"SIGNIN"] as [String : Any]
        serviceLayer.request(router: Router.verifyOTP, parameters: params) { (result: Result<OTPModel, Error>, _) in
            switch result {
            case .success(let details):
                DispatchQueue.main.async {
                    self.otpResult.onNext(details)
                }
            case .failure(let error):
                self.otpResult.onError(error)
                break
            }
        }
    }

    func resendOTP(otp: String, mobilenumber: String) {
        let params = ["mobileNumber":mobilenumber,"code": otp, "type":"SIGNIN"]
        serviceLayer.request(router: Router.resendOTP, parameters: params) { (result: Result<OTPModel, Error>, _) in
            switch result {
            case .success(let details):
                DispatchQueue.main.async {
                    self.otpResult.onNext(details)
                }
            case .failure(let error):
                self.otpResult.onError(error)
                break
            }
        }
    }

    func signUP(name: String,email: String, mobilenumber: String) {
        let params = ["name":name,"email": email, "mobileNumber":mobilenumber,"device_type": "s","device_token": "s", "type":"SIGNIN"]
        serviceLayer.request(router: Router.signUp, parameters: params) { (result: Result<LoginModel, Error>, _) in
            switch result {
            case .success(let details):
                DispatchQueue.main.async {
                    self.loginResult.onNext(details)
                }
            case .failure(let error):
                self.loginResult.onError(error)
                break
            }
        }
    }

}
