//
//  LoginModel.swift
//  GroceryBuy
//
//  Created by Shenll on 11/07/23.
//

import UIKit

struct LoginModel: Codable {
    var status: String?
    var message: String?
//    var data: LoginData?
}

struct LoginData: Codable {
    var mobileNumber: Int?
    var type: String?
}

struct OTPModel: Codable {
    var status: String?
    var message: String?
    var data: OTPData?
}

struct OTPData: Codable {
    var token: String?
}
