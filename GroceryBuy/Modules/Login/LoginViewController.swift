//
//  LoginViewController.swift
//  GroceryBuy
//
//  Created by Shenll on 11/07/23.
//

import UIKit
import RxSwift
import RxCocoa

class LoginViewController: UIViewController, StoryboardInstantiate {

    static var storyboardName: String = "Login"
    @IBOutlet weak var loginButton : UIButton!
    @IBOutlet weak var creatAccountLabel : UILabel!
    @IBOutlet weak var mobileNumberField : UITextField!

    private var viewModel = LoginViewModel()
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        creatAccountLabel.attributedText = NSAttributedString(string: "Create an account", attributes:
                                                                [.underlineStyle: NSUnderlineStyle.single.rawValue])
        self.setupHideKeyboardOnTap()
        mobileNumberField.text = "9876543210"

        loginAction()
        viewModelObservers()

    }

    func viewModelObservers(){
        viewModel.loginResult.subscribe({ [weak self] response in
            guard self != nil else {
                return
            }
            switch response {
            case let .next(res):
                switch res.status {
                case statusValue.ok.rawValue :
                    let otp: OTPViewController = OTPViewController.instantiate()
                    otp.mobileNumber = self?.mobileNumberField.text ?? ""
                    self?.navigationController?.pushViewController(otp, animated: true)
                case statusValue.error.rawValue:
                    AlertUtil.displayError(message: res.message ?? "")
                case .none:
                    break
                case .some(_):
                    break
                }
                break
            case let .error(error):
                AlertUtil.displayError(message: error.localizedDescription)
                break
            case .completed:
                break
            }
        }).disposed(by: disposeBag)

        //        let subscriptionOne = viewModel.loginResult
        //          .subscribe(onNext: { string in
        //            print("res",string)
        //          })

    }

    //MARK: - implementation of login button with RxSwift
    func loginAction() {
        loginButton.rx.tap.do(onNext:  { [unowned self] in
            self.mobileNumberField.resignFirstResponder()
        }).subscribe(onNext: { [unowned self] in
            guard let number  = mobileNumberField.text  else {return}
            //Validate mobile number field
            if self.viewModel.validateMobileNumber(value: number) {
                viewModel.loginUser(mobilenumber: number)
            }
            else {
                AlertUtil.displayError(message: "Please enter valid Mobile number")
            }
        }).disposed(by: disposeBag)
    }

}
