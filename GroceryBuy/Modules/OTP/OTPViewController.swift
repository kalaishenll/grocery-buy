//
//  OTPViewController.swift
//  GroceryBuy
//
//  Created by Shenll on 11/07/23.
//

import UIKit
import RxSwift
import RxCocoa

class OTPViewController: UIViewController, StoryboardInstantiate{
    
    static var storyboardName: String = "Login"
    @IBOutlet weak var cardView : UIView!
    @IBOutlet weak var verifyOTPButton : UIButton!
    @IBOutlet weak var resendOTPButton : UIButton!
    @IBOutlet weak var creatAccountLabel : UILabel!
    @IBOutlet weak var otp1Field : UITextField!
    @IBOutlet weak var otp2Field : UITextField!
    @IBOutlet weak var otp3Field : UITextField!
    @IBOutlet weak var otp4Field : UITextField!
    private var viewModel = LoginViewModel()
    var mobileNumber = ""
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        otp1Field.becomeFirstResponder()
        creatAccountLabel.attributedText = NSAttributedString(string: "Click Here", attributes:
            [.underlineStyle: NSUnderlineStyle.single.rawValue])
        self.setupHideKeyboardOnTap()
        self.navigationController?.isNavigationBarHidden = true
        verifyOTPAction()
    }

    //MARK: - implementation of verify OTP button
    func verifyOTPAction() {
        verifyOTPButton.rx.tap.do(onNext:  { [unowned self] in
            self.otp4Field.resignFirstResponder()
        }).subscribe(onNext: { [unowned self] in
            guard let code1  = otp1Field.text, let code2 = otp2Field.text, let code3 = otp3Field.text, let code4 = otp4Field.text  else {return}
            viewModel.verifyOTP(otp: "\(code1)\(code2)\(code3)\(code4)", mobilenumber: mobileNumber)

        }).disposed(by: disposeBag)

            viewModelObservers()
    }

    func viewModelObservers(){
        viewModel.otpResult.subscribe({ [weak self] response in
            guard self != nil else {
                return
            }
            switch response {
            case let .next(res):
                switch res.status {
                case statusValue.ok.rawValue :
                    UserDefaults.standard.setValue(true, forKey: "isLogin")
                    let otp: CustomTabBarViewController = CustomTabBarViewController.instantiate()
                    self?.navigationController?.pushViewController(otp, animated: true)
                case statusValue.error.rawValue:
                    AlertUtil.displayError(message: res.message ?? "")
                case .none:
                    break
                case .some(_):
                    break
                }
                break
            case let .error(error):
                AlertUtil.displayError(message: error.localizedDescription)
                break
            case .completed:
                break
            }
        }).disposed(by: disposeBag)
    }

}

extension OTPViewController: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            let currentString = (textField.text ?? "") as NSString
            let newString = currentString.replacingCharacters(in: range, with: string)
        if(textField == otp1Field && newString.count == 1) {
            otp1Field.text = newString
            otp2Field.becomeFirstResponder()
        }
        if(textField == otp2Field && newString.count == 1) {
            otp2Field.text = newString
            otp3Field.becomeFirstResponder()
        }
        if(textField == otp3Field && newString.count == 1) {
            otp3Field.text = newString
            otp4Field.becomeFirstResponder()
        }
            return newString.count <= 1

    }
}
