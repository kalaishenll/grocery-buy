//
//  AppDelegate.swift
//  GroceryBuy
//
//  Created by Shenll on 11/07/23.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        guard let isLogin: Bool = UserDefaults.standard.value(forKey: "isLogin") as? Bool else {
            showLogin()
            return true
        }
        if isLogin {
            showHome()
        }
        else {
            self.showLogin()
        }
        self.window?.makeKeyAndVisible()
        return true
    }

    func showLogin() {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = UINavigationController(rootViewController: LoginViewController.instantiate())
    }

    func showHome(){
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = UINavigationController(rootViewController: CustomTabBarViewController.instantiate())
//        self.window?.rootViewController = UINavigationController(rootViewController: OrderViewController.instantiate())
    }

}

